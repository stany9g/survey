﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Survey
{
    /// <summary>
    /// Interaction logic for LogicFlowControl.xaml
    /// </summary>
    public partial class LogicFlowControl : UserControl
    {
        public LogicFlowControl()
        {
            InitializeComponent();

            DataContextChanged += DataContextChangedHandler;
        }

        private void DataContextChangedHandler(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is ConditionsViewModel viewModel)
            {
                if(viewModel.SelectedAndAnswers != null)
                {
                    foreach (var value in viewModel.SelectedAndAnswers)
                    {
                        AndAnswersListBox.SelectedItems.Add(value);
                    }
                }
                if (viewModel.SelectedOrAnswers != null)
                {
                    foreach (var value in viewModel.SelectedOrAnswers)
                    {
                        OrAnswersListBox.SelectedItems.Add(value);
                    }
                }
            }
        }

        private void AnswersListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox senderListBox = sender as ListBox;
            if (DataContext is ConditionsViewModel viewModel)
            {
                var list = senderListBox.SelectedItems.Cast<AnswerViewModel>().ToList();
                if(senderListBox.Name == "AndAnswersListBox")
                {
                    viewModel.SelectedAndAnswers = new ObservableCollection<AnswerViewModel>(list);
                }
                else if(senderListBox.Name == "OrAnswersListBox")
                {
                    viewModel.SelectedOrAnswers = new ObservableCollection<AnswerViewModel>(list);
                }
            }
        }
    }
}

﻿using Microsoft.Win32;
using Survey.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Survey
{
    /// <summary>
    /// Interaction logic for RespondentAnswersControl.xaml
    /// </summary>
    public partial class RespondentAnswersControl : UserControl
    {
        public RespondentAnswersControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "CSV file (*.csv)|*.csv|Text file (*.txt)|*.txt",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                if(DataContext is RespondentAnswersListViewModel viewModel)
                {
                    viewModel.ActiveRespondent.SaveReport(saveFileDialog.FileName);
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PrintDialog dlg = new PrintDialog();
            if(dlg.ShowDialog() == true)
            {
                VisualPrinter.PrintAcrossPages(dlg, scrollViewer.Content as FrameworkElement);
            }
        }
    }
}

﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Survey
{
    /// <summary>
    /// Interaction logic for ChangePasswordControl.xaml
    /// </summary>
    public partial class ChangePasswordControl
    {
        public ChangePasswordControl()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged_Old(object sender, RoutedEventArgs e)
        {
            if(DataContext != null)
            {
                ((dynamic)DataContext).OldPass = ((PasswordBox)sender).Password;
            }
        }

        private void PasswordBox_PasswordChanged_New(object sender, RoutedEventArgs e)
        {
            if (DataContext != null)
            {
                ((dynamic)DataContext).NewPass = ((PasswordBox)sender).Password;
            }
        }

        private void PasswordBox_PasswordChanged_NewAgain(object sender, RoutedEventArgs e)
        {
            if (DataContext != null)
            {
                ((dynamic)DataContext).NewPassAgain = ((PasswordBox)sender).Password;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Survey
{
    public class MultiBooleanToVisibilitySelectConverter : MarkupExtension, IMultiValueConverter
    {
        /// <summary>
        /// A single static instance of this value converter
        /// </summary>
        private static MultiBooleanToVisibilitySelectConverter Converter = null;

        /// <summary>
        /// Provides a static instance of the value converter
        /// </summary>
        /// <param name="serviceProvider">The service provider</param>
        /// <returns></returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter ?? (Converter = new MultiBooleanToVisibilitySelectConverter());
        }


        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            // If editing mode, we want it to be shown
            if((bool)values[1] == false)
            {
                return Visibility.Visible;
            }
            if ((bool)values[0] == true)
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Survey
{
    public class MultiBooleanToVisibilityConverter : MarkupExtension, IMultiValueConverter
    {
        /// <summary>
        /// A single static instance of this value converter
        /// </summary>
        private static MultiBooleanToVisibilityConverter Converter = null;

        /// <summary>
        /// Provides a static instance of the value converter
        /// </summary>
        /// <param name="serviceProvider">The service provider</param>
        /// <returns></returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter ?? (Converter = new MultiBooleanToVisibilityConverter());
        }


        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool visible = false;
            foreach (var value in values)
            {
                if (value is bool)
                {
                    visible = visible || (bool)value;
                }
            }
            if (visible)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

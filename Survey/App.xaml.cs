﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Survey
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Custom startup so we have control across views and also can pass msg boxes
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            // Let the base application do what it needs
            base.OnStartup(e);

            MainWindow app = new MainWindow();
            bool confirm(string msg, string capt) => MessageBox.Show(msg, capt, MessageBoxButton.OK) == MessageBoxResult.OK;
            bool yesno(string msg, string capt) => MessageBox.Show(msg, capt, MessageBoxButton.YesNo) == MessageBoxResult.Yes;
            MainMenuViewModel context = new MainMenuViewModel(confirm, yesno);
            app.DataContext = context;
            app.Show();
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Survey.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    [TestClass]
    public class CircularReferenceTests
    {
        [TestMethod]
        public void CyclesInLogicTest()
        {
            var section1 = new SectionBuilder().WithId(1).Build();
            var section2 = new SectionBuilder().WithId(2).Build();
            var section3 = new SectionBuilder().WithId(3).Build();
            var section4 = new SectionBuilder().WithId(4).Build();

            var serviceDependence = new Dictionary<SectionViewModel, List<SectionViewModel>>
            {
                { section1, new List<SectionViewModel> { section2, section3 }},
                { section2, new List<SectionViewModel> { section3, section4 }},
                { section3, new List<SectionViewModel> { section1, section4 }},
                { section4, new List<SectionViewModel> { section3 }},


            };
            var cycles = serviceDependence.FindCycles();
            Assert.IsTrue(serviceDependence.FindCycles().Count == 2);
        }
    }
}

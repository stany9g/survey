﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    public class QuestionBuilder
    {
        private string _text;
        private int _id;
        private bool _required;
        private ObservableCollection<AnswerViewModel> _answers;

        public QuestionBuilder()
        {
            _text = "";
            _id = 0;
            _answers = new ObservableCollection<AnswerViewModel>();
            _required = false;
        }

        public QuestionBuilder WithText(string text)
        {
            _text = text;
            return this;
        }

        public QuestionBuilder WithId(int id)
        {
            _id = id;
            return this;
        }

        public QuestionBuilder WithAnswers(ObservableCollection<AnswerViewModel> answers)
        {
            _answers = answers;
            return this;
        }

        public QuestionViewModel Build()
        {
            var question = new QuestionViewModel
            {
                Text = _text,
                Id = _id,
                Required = _required,
                ElementsList = _answers
            };
            foreach(var answer in _answers)
            {
                answer.ParentQuestion = question;
            }
            return question;
        }
    }
}

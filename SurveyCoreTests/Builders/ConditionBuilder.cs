﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    public class ConditionBuilder
    {
        private ObservableCollection<AnswerViewModel> _selectedAndAnswers;

        private ObservableCollection<AnswerViewModel> _selectedOrAnswers;

        private SectionViewModel _toSelectedSection;

        private SectionViewModel _toElseSelectedSection;

        private bool _andCheck;

        private bool _orCheck;


        public ConditionBuilder()
        {
            _selectedAndAnswers = new ObservableCollection<AnswerViewModel>();
            _selectedOrAnswers = new ObservableCollection<AnswerViewModel>();
        }

        public ConditionBuilder WithAndSelectedAnswers(ObservableCollection<AnswerViewModel> andAnswers)
        {
            _selectedAndAnswers = andAnswers;
            return this;
        }

        public ConditionBuilder WithOrSelectedAnswers(ObservableCollection<AnswerViewModel> orAnswers)
        {
            _selectedOrAnswers = orAnswers;
            return this;
        }

        public ConditionBuilder WithToSelectedSection(SectionViewModel toSection)
        {
            _toSelectedSection = toSection;
            return this;
        }

        public ConditionBuilder WithToElseSelectedSection(SectionViewModel toElseSection)
        {
            _toElseSelectedSection = toElseSection;
            return this;
        }

        public ConditionBuilder WithAndCheck()
        {
            _andCheck = true;
            _orCheck = false;
            return this;
        }

        public ConditionBuilder WithOrCheck()
        {
            _andCheck = false;
            _orCheck = true;
            return this;
        }

        public ConditionsViewModel Build()
        {
            var condition = new ConditionsViewModel
            {
                SelectedToSection = _toSelectedSection,
                SelectedElseSection = _toElseSelectedSection,
                SelectedAndAnswers = _selectedAndAnswers,
                SelectedOrAnswers = _selectedOrAnswers,
                AndCheck = _andCheck,
                OrCheck = _orCheck
            };
            return condition;
        }
    }
}

﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    public class ActualLogicBuilder
    {
        private ObservableCollection<ConditionsViewModel> _conditions;

        public ActualLogicBuilder()
        {
            _conditions = new ObservableCollection<ConditionsViewModel>();
        }

        public ActualLogicBuilder WithConditions(ObservableCollection<ConditionsViewModel> conditions)
        {
            _conditions = conditions;
            return this;
        }

        public ActualLogicPage Build()
        {
            var logicPage = new ActualLogicPage
            {
                ElementsList = _conditions
            };
            foreach(var condition in _conditions)
            {
                condition.ActualLogicPage = logicPage;
            }
            return logicPage;
        }
    }
}

﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    public class SectionBuilder
    {
        private int _id;
        private ObservableCollection<QuestionViewModel> _questions;
        private SectionInfoViewModel _sectionInfo;
        private ActualLogicPage _logic;

        public SectionBuilder()
        {
            _id = 0;
            _questions = new ObservableCollection<QuestionViewModel>();
            _logic = new ActualLogicPage();
            _sectionInfo = new SectionInfoViewModel();
        }

        public SectionBuilder WithId(int id)
        {
            _id = id;
            return this;
        }

        public SectionBuilder WithQuestions(ObservableCollection<QuestionViewModel> questions)
        {
            _questions = questions;
            return this;
        }

        public SectionBuilder WithLogic(ActualLogicPage logic)
        {
            _logic = logic;
            return this;
        }

        public SectionBuilder WithSectionInfo(SectionInfoViewModel sectionInfo)
        {
            _sectionInfo = sectionInfo;
            return this;
        }

        public SectionViewModel Build()
        {
            var section = new SectionViewModel
            {
                Id = _id,
                ElementsList = _questions,
                SectionInfoViewModel = _sectionInfo,
                Logic = _logic
            };
            foreach(var question in _questions)
            {
                question.OwnerSection = section;
            }
            _logic.RelevantSection = section;
            return section;
        }
    }
}

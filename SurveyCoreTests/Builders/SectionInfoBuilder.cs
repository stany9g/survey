﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    public class SectionInfoBuilder
    {
        private string _name;
        private string _description;

        public SectionInfoBuilder()
        {
            _name = "";
            _description = "";
        }

        public SectionInfoBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public SectionInfoBuilder WithDescription(string description)
        {
            _description = description;
            return this;
        }

        

        public SectionInfoViewModel Build()
        {
            var sectionInfo = new SectionInfoViewModel
            {
                Name = _name,
                Description = _description
            };
            return sectionInfo;
        }
    }
}

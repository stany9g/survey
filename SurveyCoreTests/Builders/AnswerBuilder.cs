﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    public class AnswerBuilder
    {
        private string _text;
        private int _id;
        private bool _checked;

        public AnswerBuilder()
        {
            _text = "";
            _id = 0;
            _checked = false;
        }

        public AnswerBuilder WithText(string text)
        {
            _text = text;
            return this;
        }

        public AnswerBuilder WithId(int id)
        {
            _id = id;
            return this;
        }

        public AnswerViewModel Build()
        {
            var answer = new AnswerViewModel
            {
                Text = _text,
                Id = _id,
                Checked = _checked
                
            };
            return answer;
        }
    }
}

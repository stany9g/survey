﻿using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    public class SurveyBuilder
    {
        private string _text;
        private int _id;
        private ObservableCollection<SectionViewModel> _sections;
        private ObservableCollection<RespondentInfo> _respondentInfo;


        public SurveyBuilder()
        {
            _text = "";
            _id = 0;
            _sections = new ObservableCollection<SectionViewModel>();
            _respondentInfo = new ObservableCollection<RespondentInfo>();
        }

        public SurveyBuilder WithText(string text)
        {
            _text = text;
            return this;
        }

        public SurveyBuilder WithId(int id)
        {
            _id = id;
            return this;
        }

        public SurveyBuilder WithSections(ObservableCollection<SectionViewModel> sections)
        {
            _sections = sections;
            return this;
        }

        public SurveyBuilder WithRespondentInfo(ObservableCollection<RespondentInfo> respondentInfo)
        {
            _respondentInfo = respondentInfo;
            return this;
        }

        public SurveyViewModel Build()
        {
            var survey = new SurveyViewModel
            {
                Text = _text,
                Id = _id,
                ElementsList = _sections,
                RespondentInfo = _respondentInfo
            };
            foreach(var section in _sections)
            {
                section.OwnerSurvey = survey ;
            }
            foreach (var respondentInfo in _respondentInfo)
            {
                respondentInfo.OwnerSurvey = survey;
            }
            return survey;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Survey.Core;

namespace SurveyCoreTests
{
    [TestClass]
    public class EnumeratorTests
    {

        [TestMethod]
        public void EnumeratorTextTest()
        {
            // Arrange
            List<string> expectedStrings = new List<string>
            {
                "Sekce 1/4",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionA",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionB",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionC",
                "Sekce 2/4",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionA",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionB",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionC",
                "Sekce 3/4",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionA",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionB",
                "OptionA",
                "OptionB",
                "OptionC",
                "QuestionC",
                "Sekce 4/4",
            };
            BaseData baseData = new BaseData();
            int i = 0;

            // Act & Assert
            foreach (var element in baseData.SurveyViewModel)
            {
                Assert.AreEqual(expectedStrings[i], element.Text);
                i++;
            }
        }
    }
}

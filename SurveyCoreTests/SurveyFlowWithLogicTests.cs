﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;
using Survey.Core;

namespace SurveyCoreTests
{
    [TestClass]
    public class SurveyFlowWithLogicTests
    {
        [TestMethod]
        public void NextSectionTest()
        {
            // Arrange
            var answerA = new AnswerBuilder().WithId(1).WithText("OptionA").Build();
            var answerB = new AnswerBuilder().WithId(1).WithText("OptionB").Build();
            var answerC = new AnswerBuilder().WithId(1).WithText("OptionC").Build();
            var answersFirst = new ObservableCollection<AnswerViewModel>
            {
                answerA,
                answerB,
                answerC
            };

            var answerD = new AnswerBuilder().WithId(1).WithText("OptionD").Build();
            var answerE = new AnswerBuilder().WithId(1).WithText("OptionE").Build();
            var answerF = new AnswerBuilder().WithId(1).WithText("OptionF").Build();

            var answersSecond = new ObservableCollection<AnswerViewModel>
            {
                answerD,
                answerE,
                answerF
            };

            var questions = new ObservableCollection<QuestionViewModel>
            {
                new QuestionBuilder().WithId(1).WithText("QuestionA").WithAnswers(answersFirst).Build(),
                new QuestionBuilder().WithId(2).WithText("QuestionB").WithAnswers(answersSecond).Build()
            };
            var section3 = new SectionBuilder().WithId(3).WithQuestions(questions).Build();
            var section4 = new SectionBuilder().WithId(4).WithQuestions(questions).Build();

            var answersAndSelected = new ObservableCollection<AnswerViewModel>
            {
                answerA,
                answerB,
                answerD
            };

            var answersOrSelected = new ObservableCollection<AnswerViewModel>
            {
                answerF,
            };

            var condition = new ConditionBuilder()
                .WithAndSelectedAnswers(answersAndSelected)
                .WithOrSelectedAnswers(answersOrSelected)
                .WithToSelectedSection(section3)
                .WithToElseSelectedSection(section4)
                .WithOrCheck()
                .Build();

            var actualLogicPage = new ActualLogicBuilder().WithConditions(new ObservableCollection<ConditionsViewModel> { condition }).Build();

            var section2 = new SectionBuilder().WithId(2).WithQuestions(questions).WithLogic(actualLogicPage).Build();

            ObservableCollection<SectionViewModel> sections = new ObservableCollection<SectionViewModel>
            {
                section2,
                section3,
                section4
            };


            var survey = new SurveyBuilder().WithSections(sections).Build();
            var logic = new LogicFlowViewModel(survey);
            actualLogicPage.LogicOwner = logic;
            logic.IsValidLogic = true;
            // Act expected section3
            answerA.Checked = true;
            answerB.Checked = true;
            answerD.Checked = true;
           var actualSection = actualLogicPage.GetNextSection();

            // Assert
            Assert.AreEqual(section3, actualSection);

            // Act expected section4
            answerA.Checked = false;
            actualSection = actualLogicPage.GetNextSection();

            // Assert
            Assert.AreEqual(section4, actualSection);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    [TestClass]
    public class ComparatorByRespondentAnswerTests
    {
        [TestMethod]
        public void CompareEqualTest()
        {
            // Arrange
            ObservableCollection<RespondentInfo> respondentInfos = new ObservableCollection<RespondentInfo>
            {
                CreateRespondentInfos("Name","Petr"),
                CreateRespondentInfos("Surname","Hodny"),
                CreateRespondentInfos("Address","Prague"),
                CreateRespondentInfos("Number","77224"),
            };

            // Act
            var result = Compare(respondentInfos, respondentInfos);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CompareNotEqualTestByLengthTest()
        {
            // Arrange
            ObservableCollection<RespondentInfo> respondentInfos1 = new ObservableCollection<RespondentInfo>
            {
                CreateRespondentInfos("Name","Petr"),
                CreateRespondentInfos("Surname","Hodny"),
                CreateRespondentInfos("Address","Prague"),
                CreateRespondentInfos("Number","77224"),
            };
            ObservableCollection<RespondentInfo> respondentInfos2 = new ObservableCollection<RespondentInfo>
            {
                CreateRespondentInfos("Name","Petr"),
                CreateRespondentInfos("Surname","Hodny"),
                CreateRespondentInfos("Address","Prague"),
            };

            // Act
            var result = Compare(respondentInfos1, respondentInfos2);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void CompareNotEqualByTextTest()
        {
            // Arrange
            ObservableCollection<RespondentInfo> respondentInfos1 = new ObservableCollection<RespondentInfo>
            {
                CreateRespondentInfos("Name","Petr"),
                CreateRespondentInfos("Surname","Hodny"),
                CreateRespondentInfos("Address","Prague"),
                CreateRespondentInfos("Number","77224"),
            };
            ObservableCollection<RespondentInfo> respondentInfos2 = new ObservableCollection<RespondentInfo>
            {
                CreateRespondentInfos("Name","Petr"),
                CreateRespondentInfos("Surname","Divoký"),
                CreateRespondentInfos("Address","Prague"),
                CreateRespondentInfos("Number","772"),
            };

            // Act
            var result = Compare(respondentInfos1, respondentInfos2);

            // Assert
            Assert.IsFalse(result);
        }

        private RespondentInfo CreateRespondentInfos(string labelText, string respondentText)
        {
            var respondentInfo = new RespondentInfo
            {
                LabelText = labelText,
                RespondentText = respondentText
            };
            return respondentInfo;
        }

        private bool Compare(ObservableCollection<RespondentInfo> respondentInfos1, ObservableCollection<RespondentInfo> respondentInfos2)
        {
            var respondentAnswer1 = new RespondentAnswers
            {
                RespondentInfo = respondentInfos1
            };
            var respondentAnswer2 = new RespondentAnswers
            {
                RespondentInfo = respondentInfos2
            };
            var comparer = new ComparatorByRespondentAnswer();
            var result = comparer.Compare(respondentAnswer1, respondentAnswer2);

            // Assert
            if (result == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

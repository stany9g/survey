﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Survey.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    [TestClass]
    public class BaseFunctionsTests
    {
        [TestMethod]
        public void AddElementTest()
        {
            BaseData baseData = new BaseData();
            foreach(var element in baseData.SurveyViewModel)
            {
               if(element is QuestionViewModel)
                {
                    var question = element as QuestionViewModel;
                    int expectedNumberOfElements = question.ElementsList.Count();
                    question.AddElementCommand.Execute(question);
                    int actualNumberOfElements = question.ElementsList.Count();
                    Assert.AreEqual(expectedNumberOfElements + 1, actualNumberOfElements, "Answers in question - Add");
                }
                if (element is SectionViewModel)
                {
                    var section = element as SectionViewModel;
                    int expectedNumberOfElements = section.ElementsList.Count();
                    section.AddElementCommand.Execute(section);
                    int actualNumberOfElements = section.ElementsList.Count();
                    Assert.AreEqual(expectedNumberOfElements + 1, actualNumberOfElements, "Questions in section - Add");
                }
                if (element is SurveyViewModel)
                {
                    var survey = element as SurveyViewModel;
                    int expectedNumberOfElements = survey.ElementsList.Count();
                    survey.AddElementCommand.Execute(survey);
                    int actualNumberOfElements = survey.ElementsList.Count();
                    Assert.AreEqual(expectedNumberOfElements + 1, actualNumberOfElements, "Sections in survey - Add");
                }
            }
        }

        [TestMethod]
        public void RemoveElementTest()
        {
            BaseData baseData = new BaseData();
            foreach (var element in baseData.SurveyViewModel)
            {
                if (element is QuestionViewModel)
                {
                    var question = element as QuestionViewModel;
                    int expectedNumberOfElements = question.ElementsList.Count();
                    if (expectedNumberOfElements > 0)
                    {
                        var elementToDelete = question.ElementsList.Last();
                        question.RemoveElementCommand.Execute(elementToDelete);
                        int actualNumberOfElements = question.ElementsList.Count();
                        Assert.AreEqual(expectedNumberOfElements - 1, actualNumberOfElements, "Answers in question - Remove");
                    }
                }
                if (element is SectionViewModel)
                {
                    var section = element as SectionViewModel;
                    int expectedNumberOfElements = section.ElementsList.Count();
                    if(expectedNumberOfElements > 0)
                    {
                        var elementToDelete = section.ElementsList.Last();
                        section.RemoveElementCommand.Execute(elementToDelete);
                        int actualNumberOfElements = section.ElementsList.Count();
                        Assert.AreEqual(expectedNumberOfElements - 1, actualNumberOfElements, "Questions in section - Remove");
                    }
                }
            }
        }
    }
}

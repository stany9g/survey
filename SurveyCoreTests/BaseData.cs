﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Survey.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyCoreTests
{
    [TestClass]
    public class BaseData
    {
        public  SurveyViewModel SurveyViewModel { get; private set; }

        public BaseData()
        {
            Init();
        }

        private  void Init()
        {
            var answers = new ObservableCollection<AnswerViewModel>
            {
                new AnswerBuilder().WithId(1).WithText("OptionA").Build(),
                new AnswerBuilder().WithId(2).WithText("OptionB").Build(),
                new AnswerBuilder().WithId(3).WithText("OptionC").Build()
            };
            var questions = new ObservableCollection<QuestionViewModel>
            {
                new QuestionBuilder().WithId(1).WithText("QuestionA").WithAnswers(answers).Build(),
                new QuestionBuilder().WithId(2).WithText("QuestionB").WithAnswers(answers).Build(),
                new QuestionBuilder().WithId(3).WithText("QuestionC").WithAnswers(answers).Build()
            };
            var sections = new ObservableCollection<SectionViewModel>
            {
                new SectionBuilder().WithId(1).Build(),
                new SectionBuilder().WithId(2).WithQuestions(questions).Build(),
                new SectionBuilder().WithId(3).WithQuestions(questions).Build(),
                new SectionBuilder().WithId(4).WithQuestions(questions).Build()
            };
            SurveyViewModel =  new SurveyBuilder().WithId(1).WithSections(sections).Build();
        }
    }
}

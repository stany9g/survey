﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Windows;

namespace Survey.Core
{
    public class DataSerialization
    {
        private static readonly string  passPhrase = "NaplavaJeKing";

        public static IEnumerable<SurveyViewModel> Load()
        {
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\SurveysData";
            try
            {
                var files = Directory.GetFiles(dir, "*.json", SearchOption.TopDirectoryOnly);
                var returnList = new List<SurveyViewModel>();
                foreach (var file in files)
                {
                    string output = File.ReadAllText(file);
                    string decrypted = Encryption.Decrypt(output, passPhrase);
                    returnList.Add(JsonConvert.DeserializeObject<SurveyViewModel>(decrypted, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Error }));
                }
                return returnList;
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Adresář: 'SurveysData' neexistuje, bude automaticky vytvořen ve vašich Dokumentech.");
                Directory.CreateDirectory(dir);
                return new List<SurveyViewModel>();
            }
        }

        public static void Save(SurveyViewModel survey)
        {
            string output = JsonConvert.SerializeObject(survey, Formatting.Indented,
            new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Error });
            string encrypted = Encryption.Encrypt(output, passPhrase);
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\SurveysData";
            Directory.CreateDirectory(dir);
            string filePath = Path.Combine(dir, survey.Text + " " + survey.Uuid  + ".json");
            File.WriteAllText(filePath, encrypted);
        }
    }
}

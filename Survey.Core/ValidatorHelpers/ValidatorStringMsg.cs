﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class ValidatorStringMsg
    {
        private StringBuilder _sb;

        public ValidatorStringMsg()
        {
            _sb = new StringBuilder();
        }

        public void CreateCreatingMsg(IElement element)
        {
            if(_sb.Length == 0)
            {
                string defaultMsg = "Dotazník není vyplněn správně. Zkontrolujte si, že žádná z otázek ani možností odpovědí nemá červenou barvu. Bližší info:";
                _sb.AppendLine(defaultMsg);
            }
            
            string msg = "";
            if(element as AnswerViewModel != null)
            {
                var answer = element as AnswerViewModel;
                msg = $"Odpověd číslo {answer.Id} u otázky s ID {answer.ParentQuestion.Id} neobsahuje žádný text.";
            }
            else if(element as QuestionViewModel != null)
            {
                msg = $"Otázka s ID: {(element as QuestionViewModel).Id} neobsahuje žádný text nebo neobsahuje alespoň 2 možnosti pro odpověď.";
            }
            else if (element as SectionViewModel != null)
            {
                var section = element as SectionViewModel;
                if(section.IsMainSection)
                {
                    msg = $"Chybí název dotazníku, který se vyplňuje v první sekci";
                }
                else
                {
                    msg = $"Každá sekce musí mít alespoň jednu otázku.";
                }

            }
            else if (element as SurveyViewModel != null)
            {
                var survey = element as SurveyViewModel;
                if(survey.ElementsList.Count == 1)
                {
                    msg = $"Dotazník neobsahuje žádné další sekce s otázkami.";
                }
                else
                {
                    msg = $"U otázky pro osobní informaci od respondenta chybí text.";
                }
            }
            _sb.AppendLine(msg);
        }

        public void CreateLogicMsg(string text)
        {
            _sb.AppendLine(text);     
        }

        public override string ToString()
        {
            return _sb.ToString();
        }
    }
}

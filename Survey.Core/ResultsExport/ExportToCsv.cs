﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class ExportToCsv
    {
        public static void CreateCsv(string filePath, RespondentAnswers respondent)
        {
            var records = PrepareForCreate(respondent);

            using (var writer = new StreamWriter(filePath,false,Encoding.UTF8))
            using (var csv = new CsvWriter(writer))
            {
                csv.WriteRecords(records);
            }
        }

        private static List<CsvData> PrepareForCreate(RespondentAnswers respondent)
        {
            var records = new List<CsvData>();
            CsvData csv = new CsvData
            {
                Question = "Název dotazníku",
                Answer = respondent.OwnerList.OwnerSurvey.Text
            };
            records.Add(csv);
            csv = new CsvData
            {
                Question = "Popis",
                Answer = respondent.OwnerList.OwnerSurvey.Description
            };
            records.Add(csv);
            respondent.RespondentInfo.ToList().ForEach(x =>
            {

                csv = new CsvData
                {
                    Question = x.LabelText,
                    Answer = x.RespondentText
                };
                records.Add(csv);
            });
            respondent.WalkTrought
                .SelectMany(x => x.ElementsList)
                .Where(x => x.InReport == true)
                .ToList().ForEach(x =>
                {
                    csv = new CsvData
                    {
                        Question = x.Text,
                        Answer = string.Join(Environment.NewLine, x.ElementsList.Where(y => y.Checked == true).Select(y => y.Text))
                    };
                    records.Add(csv);
                });

            return records;
        }
    }

    public class CsvData
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}

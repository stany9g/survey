﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class SectionViewModel : BaseDataModel<QuestionViewModel>
    {
        [JsonProperty]
        public override string Text
        {
            get
            {
                return $"Sekce {Id}/{OwnerSurvey.ElementsList.Count}";
            }
        }

        public override bool IsValid
        {
            get
            {
                if (IsMainSection)
                {
                    return !string.IsNullOrEmpty(SectionInfoViewModel.Name);
                }
                return ElementsList.Count > 0;
            }
        }

        public SurveyViewModel OwnerSurvey { get; set; }

        [JsonProperty]
        public SectionInfoViewModel SectionInfoViewModel { get; set; }

        public string Name
        {
            get
            {
                if(SectionInfoViewModel != null)
                {
                    return SectionInfoViewModel.Name;
                }
                return "";
            }
        }

        public ActualLogicPage Logic { get; set; }

        public ICommand BackToStartCommand { get; set; }

        [JsonProperty]
        public bool IsMainSection { get; set; }


        public SectionViewModel(SurveyViewModel survey) : base()
        {
            OwnerSurvey = survey;
            if (OwnerSurvey.ElementsList.Count == 0)
            {
                IsMainSection = true;
                OwnerSurvey.RespondentInfo.Add(new RespondentInfo(OwnerSurvey)
                {
                    LabelText = "Jméno:"
                });
            }
            else
            {
                IsMainSection = false;
            }
            Init();
        }

        public SectionViewModel()
        {
            Init();
        }

        private void BackToStart()
        {
            if (EditingHelper.Instance.Respondent)
            {
                bool res = OwnerSurvey.OwnerSurveyManager.MainViewModel.GetYesOrNo("Opravdu chcete ukončit vyplňování?", "Vaše odpovědi budou ztraceny.");
                if (res)
                {
                    OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = new StartOfSurveyViewModel(OwnerSurvey.OwnerSurveyManager);
                }
            }
            else
            {
                var answers = OwnerSurvey.ElementsList
                .SelectMany(x => x.ElementsList)
                .SelectMany(x => x.ElementsList)
                .Where(x => x.Checked == true)
                .ToList();
                answers.ForEach(x => x.Checked = false);
                OwnerSurvey.RespondentInfo.ToList().ForEach(x => x.RespondentText = "");
                EditingHelper.Instance.Editing = false;

                OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = OwnerSurvey.OwnerSurveyManager;
            }
        }



        private void Init()
        {
            if (SectionInfoViewModel == null)
            {
                if (IsMainSection)
                {
                    SectionInfoViewModel = new SectionInfoViewModel("Název dotazníku", "Popis dotazníku", this);
                }
                else
                {
                    SectionInfoViewModel = new SectionInfoViewModel("Název sekce", "Popis (nepovinný)", this);
                }
            }
            BackToStartCommand = new RelayCommand(BackToStart);
            Logic = new ActualLogicPage { RelevantSection = this };

        }

        public override string ToString()
        {
            return $"Sekce {Id}";
        }

    }
}

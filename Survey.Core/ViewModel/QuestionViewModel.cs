﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class QuestionViewModel : BaseDataModel<AnswerViewModel>
    {
        #region Interface
        [JsonProperty]
        public bool Required { get; set; }

        [JsonProperty]
        public bool InReport { get; set; }

        public bool RequiredHasAnswerCheck
        {
            get
            {
                if (Required)
                {
                    return !ElementsList.Any(x => x.Checked);
                }
                return false;
            }
        }


        public SectionViewModel OwnerSection { get; set; }


        #endregion


        public override bool IsValid
        {
            get
            {
                // For validity there must be two answers for every question to make sense
                return !string.IsNullOrEmpty(Text) && ElementsList.Count > 1;
            }
        }

        public QuestionViewModel(SectionViewModel owner) : base()
        {
            OwnerSection = owner;

            // Creating with one default empty answer
            AddElement(this);
        }

        public QuestionViewModel()
        {

        }

        public void UpdateRequired()
        {
            OnPropertyChanged(nameof(RequiredHasAnswerCheck));
        }

    }
}

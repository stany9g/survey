﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class SurveyViewModel : BaseDataModel<SectionViewModel>, IEnumerable<IElement>
    {
        #region Public
        [JsonProperty]
        public ObservableCollection<RespondentInfo> RespondentInfo { get; set; } = new ObservableCollection<RespondentInfo>();

        [JsonProperty]
        public ObservableCollection<RespondentAnswers> RespondentAnswers { get; set; } = new ObservableCollection<RespondentAnswers>();

        [JsonProperty]
        public string Uuid { get; private set; }

        public bool ResultNumber
        {
            get
            {
                return RespondentAnswers.Count > 0;
            }
        }

        private bool _readyForRespondent = false;

        public bool ReadyForRespondent
        {
            get
            {
                return _readyForRespondent;
            }
            set
            {
                _readyForRespondent = value;
                if(OwnerSurveyManager != null)
                {
                    foreach (var survey in OwnerSurveyManager.ElementsList)
                    {
                        survey.UpdateProperty();
                    }
                    DataSerialization.Save(this);
                }
            }
        }

        public bool IsOnly
        {
            get
            {
                foreach(var survey in OwnerSurveyManager.ElementsList)
                {
                    if (survey.ReadyForRespondent && survey != this)
                    {
                        return false;
                    }
                }
                return true;
            }
        }


        [JsonProperty]
        public LogicFlowViewModel Logic { get; set; }

        public SectionViewModel ActiveSection { get; set; }

        public string Description
        {
            get
            {
                var sectionViewModel = ElementsList.First() as SectionViewModel;
                var description = sectionViewModel.SectionInfoViewModel.Description;
                if (string.IsNullOrEmpty(description))
                {
                    return "Bez popisu";
                }
                return $"Popis:{Environment.NewLine}{description}";
            }
        }

        public SurveyManagerViewModel OwnerSurveyManager { get; set; }

        public string NumberOfResponseText
        {
            get
            {
                return $"Počet vyplnění: {RespondentAnswers.Count}";
            }
        }

        public string ReadyToFillText
        {
            get
            {
                if (IsValid)
                {
                    return "Dotazník připraven k vyplnění";
                }
                return "Dotazník není připraven k vyplnění";
            }
        }

        public string ReadyLogicText
        {
            get
            {
                if(Logic != null)
                {
                    if (!Logic.IsValidLogic)
                    {
                        return "Logický průchod není validní.";
                    }
                }
                return "";
            }
        }

        #endregion

        #region Implementation of Interface


        private ValidatorStringMsg _validatorStringMsg;
        public override bool IsValid
        {
            get
            {
                _validatorStringMsg = new ValidatorStringMsg();
                var valid = true;
                foreach (var respondent in RespondentInfo)
                {
                    if (string.IsNullOrEmpty(respondent.LabelText))
                    {
                        valid = false;
                        _validatorStringMsg.CreateCreatingMsg(this);
                    }
                }
                foreach (var element in this)
                {
                    if (!element.IsValid)
                    {
                        valid = false;
                        _validatorStringMsg.CreateCreatingMsg(element);
                    }
                }
                if(ElementsList.Count == 1)
                {
                    valid = false;
                    _validatorStringMsg.CreateCreatingMsg(this);
                }
                return valid;
            }
        }

        public override string Text
        {
            get
            {
                var sectionViewModel = ElementsList.First();
                var name = sectionViewModel.SectionInfoViewModel.Name;
                if (string.IsNullOrEmpty(name))
                {
                    return "Bez názvu";
                }
                return name;
            }
        }

        #endregion

        #region Public Commands

        public ICommand AddRespondentInfoCommand { get; private set; }

        public ICommand DeleteRespondentInfoCommand { get; private set; }

        public ICommand LeftSectionCommand { get; private set; }

        public ICommand RightSectionCommand { get; private set; }

        public ICommand NextSectionCommand { get; set; }

        #endregion

        #region Constructor

        public SurveyViewModel(SurveyManagerViewModel surveyManager) : base()
        {
            OwnerSurveyManager = surveyManager;
            Uuid = Guid.NewGuid().ToString();
            InitCommands();
            // When creating a new survey, starting with first section already
            AddElement(this);
        }

        public SurveyViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            LeftSectionCommand = new RelayCommand(LeftSection);
            RightSectionCommand = new RelayCommand(RightSection);
            AddRespondentInfoCommand = new RelayCommand(AddRespondentInfo);
            DeleteRespondentInfoCommand = new RelayParameterizedCommand(parameter => DeleteRespondentInfo(parameter));
            NextSectionCommand = new RelayParameterizedCommand(parameter => NextSection(parameter));
        }
        private List<SectionViewModel> _walkThrough = new List<SectionViewModel>();
        private void NextSection(object parameter)
        {
            var section = parameter as SectionViewModel;

            if (section.IsMainSection)
            {
                if (!RespondentInfo.Any(x => x.RequiredHasAnswerCheck))
                {
                    _walkThrough.Add(section);
                    RightSection();
                }
            }
            else
            {
                if (!section.ElementsList.Any(x => x.RequiredHasAnswerCheck))
                {
                    _walkThrough.Add(ActiveSection);
                    ActiveSection = section.Logic.GetNextSection();
                }
            }
            if (ActiveSection == null)
            {
                OwnerSurveyManager.MainViewModel.CurrentPageViewModel = new EndOfSurveyViewModel(this, _walkThrough);
                _walkThrough = new List<SectionViewModel>();
            }
        }


        #endregion

        #region Private helper methods

        public void UpdateProperty()
        {
            OnPropertyChanged(nameof(IsOnly));
            OnPropertyChanged(nameof(ResultNumber));
            OnPropertyChanged(nameof(NumberOfResponseText));

        }

        private void DeleteRespondentInfo(object parameter)
        {
            RespondentInfo.Remove(parameter as RespondentInfo);
            OnPropertyChanged(nameof(IsValid));

        }

        private void AddRespondentInfo()
        {
            RespondentInfo.Add(new RespondentInfo(this));
            OnPropertyChanged(nameof(IsValid));
        }

        private void RightSection()
        {
            var indexOf = ElementsList.IndexOf(ActiveSection);
            if (indexOf < ElementsList.Count-1)
            {
                ActiveSection = ElementsList.ElementAt(indexOf + 1);
            }
        }

        private void LeftSection()
        {
            var indexOf = ElementsList.IndexOf(ActiveSection);
            if(indexOf > 0)
            {
                ActiveSection = ElementsList.ElementAt(indexOf - 1);
            }
        }

        protected override void AddElement(object parameter)
        {
            var element = new SectionViewModel(this);
            ElementsList.Add(element);
            UpdateId();
            ActiveSection = element;
            var survey = GetSurvey(parameter);
            if (survey != null)
            {
                DataSerialization.Save(survey);
            }
        }

        protected override void RemoveElement(object parameter)
        {
            var section = parameter as SectionViewModel;
            var indexOfDeleted = ElementsList.IndexOf(section);
            ElementsList.Remove(section);
            UpdateId();
            if (indexOfDeleted < ElementsList.Count)
            {
                ActiveSection = ElementsList.ElementAt(indexOfDeleted);
            }
            else
            {
                ActiveSection = ElementsList.Last();
            }
            var survey = GetSurvey(parameter);
            if (survey != null)
            {
                DataSerialization.Save(survey);
            }
        }



        #endregion

        #region Enumerator

        public IEnumerator<IElement> GetEnumerator()
        {
            foreach (var section in ElementsList)
            {
                foreach (var question in section.ElementsList)
                {
                    foreach (var answer in question.ElementsList)
                    {
                        yield return answer;
                    }
                    yield return question;
                }
                yield return section;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Public methods

        public string GetErrorMsg()
        {
            if (_validatorStringMsg != null)
            {
                return _validatorStringMsg.ToString();

            }
            return null;
        }

        public void SetOwnersLogicRight()
        {
            if (Logic != null)
            {
                Logic.OwnerSurvey = this;
                Logic.LogicPage = Logic.ConditionPages.First();
                foreach (var actualLogicPage in Logic.ConditionPages)
                {
                    actualLogicPage.LogicOwner = Logic;
                    actualLogicPage.RelevantSection.Logic = actualLogicPage;
                    foreach (var condition in actualLogicPage.ElementsList)
                    {
                        condition.ActualLogicPage = actualLogicPage;
                    }
                }
            }
        }

        #endregion

    }
}

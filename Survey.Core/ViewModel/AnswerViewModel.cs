﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public class AnswerViewModel : BaseDataModel<AnswerViewModel>
    {
        private bool _checked;
        public bool Checked
        {
            get
            {
                return _checked;
            }
            set
            {
                _checked = value;
                ParentQuestion.UpdateRequired();
            }
        }

        public QuestionViewModel ParentQuestion { get; set; }

        public override bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Text);
            }
        }

        public AnswerViewModel(QuestionViewModel ownerQuestion) : base()
        {
            ParentQuestion = ownerQuestion;
        }

        public AnswerViewModel()
        {

        }

        private AnswerViewModel(QuestionViewModel parent, bool checkedAn, int id, string text)
        {
            ParentQuestion = parent;
            Checked = checkedAn;
            Id = id;
            Text = text;
        }

        public AnswerViewModel DeepCopy(AnswerViewModel source)
        {
            return new AnswerViewModel(source.ParentQuestion, source.Checked, source.Id, source.Text);
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BaseDataModel<T> : BaseViewModel, IElement where T : IElement
    {
        [JsonProperty]
        public ObservableCollection<T> ElementsList { get; set; } = new ObservableCollection<T>();

        [JsonProperty]
        public virtual int Id { get; set; }

        [JsonProperty]
        public virtual string Text { get; set; }

        public virtual bool IsValid { get; }

        public ICommand AddElementCommand { get; private set; }

        public ICommand RemoveElementCommand { get; private set; }

        protected virtual void AddElement(object parameter)
        {
            var element = (T)Activator.CreateInstance(typeof(T), parameter);
            ElementsList.Add(element);
            UpdateId();
            var survey = GetSurvey(parameter);
            if(survey != null)
            {
                DataSerialization.Save(survey);
            }

        }

        protected virtual void RemoveElement(object parameter)
        {
            ElementsList.Remove((T)parameter);
            UpdateId();
            var survey = GetSurvey(parameter);
            if (survey != null)
            {
                DataSerialization.Save(survey);
            }
        }

        public BaseDataModel()
        {
            AddElementCommand = new RelayParameterizedCommand(parameter => AddElement(parameter));
            RemoveElementCommand = new RelayParameterizedCommand(parameter => RemoveElement(parameter));
        }


        public void UpdateId()
        {
            foreach (var element in ElementsList)
            {
                element.Id = ElementsList.IndexOf(element) + 1;
            }
        }

        protected SurveyViewModel GetSurvey(object parameter)
        {
            SurveyViewModel survey = null;
            if(parameter is AnswerViewModel answer)
            {
                survey = answer.ParentQuestion.OwnerSection.OwnerSurvey;
            }
            else if(parameter is QuestionViewModel question)
            {
                survey = question.OwnerSection.OwnerSurvey;
            }
            else if(parameter is SectionViewModel section)
            {
                survey = section.OwnerSurvey;
            }
            if(parameter is SurveyViewModel survey1)
            {
                survey = survey1;
            }
            return survey;
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ConditionsViewModel : BaseDataModel<ConditionsViewModel>
    {
        [JsonProperty]
        public bool EndSectionCheck { get; set; }

        [JsonProperty]
        public bool ElseEndSectionCheck { get; set; }

        [JsonProperty]
        public bool AndCheck { get; set; }

        [JsonProperty]
        public bool OrCheck { get; set; }

        public bool IsEnabledChecks
        {
            get
            {
                if(SelectedAndAnswers.Count() == 0 || SelectedOrAnswers.Count() == 0)
                {
                    return false;
                }
                return true;
            }
        }

        public bool EnabledLastCondition
        {
            get
            {
                if (this == ActualLogicPage.ElementsList.Last() && !ElseEndSectionCheck)
                {
                    return true;
                }
                return false;
            }
        }

        public ActualLogicPage ActualLogicPage { get; set; }

        private ObservableCollection<AnswerViewModel> _allTheAnswers;

        public ObservableCollection<AnswerViewModel> AllTheAnswersInSection
        {
            get
            {
                if(_allTheAnswers == null)
                {
                    var answers = ActualLogicPage.RelevantSection.ElementsList.SelectMany(x => x.ElementsList).ToList();
                    _allTheAnswers = new ObservableCollection<AnswerViewModel>(answers);
                    return _allTheAnswers;
                }
                return _allTheAnswers;
            }
        }

        private ObservableCollection<SectionViewModel> _toSection;

        public ObservableCollection<SectionViewModel> ToSection
        {
            get
            {
                    var sections = ActualLogicPage.RelevantSection.OwnerSurvey.ElementsList;
                    var relevantSections = sections.Where(x => x.IsMainSection == false && x != ActualLogicPage.RelevantSection);
                    _toSection = new ObservableCollection<SectionViewModel>(relevantSections);
                    return _toSection;
            }
        }

        public ObservableCollection<SectionViewModel> ElseSection
        {
            get
            {
                var sections = ActualLogicPage.RelevantSection.OwnerSurvey.ElementsList;
                var relevantSections = sections.Where(x => x.IsMainSection == false && x != ActualLogicPage.RelevantSection && x != SelectedToSection);
                _toSection = new ObservableCollection<SectionViewModel>(relevantSections);
                return _toSection;

            }
        }

        [JsonProperty]
        private SectionViewModel _selectedToSection;

        public SectionViewModel SelectedToSection
        {
            get
            {
                if (EndSectionCheck)
                {
                    _selectedToSection = null;
                }
                return _selectedToSection;
            }
            set
            {
                _selectedToSection = value;
                OnPropertyChanged(nameof(ElseSection));
            }
        }

        [JsonProperty]
        private SectionViewModel _selectedElseSection;

        public SectionViewModel SelectedElseSection
        {
            get
            {
                if(ElseEndSectionCheck && ActualLogicPage.ElementsList.Count() == 1)
                {
                    _selectedElseSection = null;
                }
                return _selectedElseSection;
            }
            set
            {
                _selectedElseSection = value;
            }
        }

        [JsonProperty]
        public ObservableCollection<AnswerViewModel> SelectedAndAnswers { get; set; } = new ObservableCollection<AnswerViewModel>();

        [JsonProperty]
        public ObservableCollection<AnswerViewModel> SelectedOrAnswers { get; set; } = new ObservableCollection<AnswerViewModel>();


        private ValidatorStringMsg _validatorMsg;

        public override bool IsValid
        {
            get
            {
                _validatorMsg = new ValidatorStringMsg();
                var valid = true;
                string msg = $"Na stránce: { ActualLogicPage.RelevantSection.Text}, podmínka číslo: {Id}";
                _validatorMsg.CreateLogicMsg(msg);
                if (SelectedToSection == null && EndSectionCheck == false)
                {
                    valid = false;
                     msg = $"Není vybrána sekce v listu 'Do sekce', do které se má přejít v případě splněné podmínky. ";
                    _validatorMsg.CreateLogicMsg(msg);
                }
                if (_selectedElseSection == null && ElseEndSectionCheck == false && this == ActualLogicPage.ElementsList.Last())
                {
                    valid = false;
                    msg = $"Není vybrána sekce v seznamu 'Jinak', do které se má přejít v případě nesplněné podmínky.";
                    _validatorMsg.CreateLogicMsg(msg);
                }
                if((SelectedAndAnswers == null || SelectedAndAnswers.Count == 0) && (SelectedOrAnswers == null || SelectedOrAnswers.Count == 0))
                {
                    valid = false;
                    msg = $"Nejsou vybrané žádné možnosti ani v seznamu AND ani v seznamu OR.";
                    _validatorMsg.CreateLogicMsg(msg);
                }
                return valid;
            }
        }

        public ConditionsViewModel(ActualLogicPage ownerLogicPage)
        {
            ActualLogicPage = ownerLogicPage;
        }

        public ConditionsViewModel()
        {

        }

        public void UpdateProperty()
        {
           OnPropertyChanged(nameof(EnabledLastCondition));
        }

        public string GetErrorMessage()
        {
            return _validatorMsg.ToString();
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ActualLogicPage : BaseDataModel<ConditionsViewModel>
    {
        [JsonProperty]
        public SectionViewModel RelevantSection { get;  set; }

        public LogicFlowViewModel LogicOwner { get; set; }

        public ICommand HelpCommand { get; set; }

        public ActualLogicPage(LogicFlowViewModel owner, SectionViewModel section)
        {
            LogicOwner = owner;
            RelevantSection = section;
            AddElement(this);

            HelpCommand = new RelayCommand(Help);
        }



        public ActualLogicPage()
        {
            HelpCommand = new RelayCommand(Help);
        }

        private void Help()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Jak rozumět následujícímu okno:");
            sb.AppendLine("Tabulka 'Do sekce' znamená, že pokud bude splněna podmínka, tak následující sekce bude právě tato zvolená.");
            sb.AppendLine("Tabulka 'Pokud AND' zde jsou všechny podmínky ve vztahu 'a zároveň', čili všechny odpovědi, které zde zvolíte musí" +
                " být zaškrtnuty, aby byla podmínka splněna.");
            sb.AppendLine("Tabulka 'Pokud OR' zde jsou všechny podmínky ve vztahu 'nebo', čili stačí pokud bude alespoň jedna ze zvolených" +
                " možností zaškrtnuta respondentem a bude podmínka splněna.");
            sb.AppendLine("Tyto dvě tabulky spolu jdou kombinovat. A to pomocí tlačítko mezi těmito tabulky, kde můžete zvolit jestli" +
                " tyto dvě podmínky budou ve vztahu 'AND - a zároveň' čili podmínky z obou těchto tabulek musí být splněny nebo" +
                " můžete zvolit 'OR - nebo', kdy tedy stačí, aby alespoň jedna z těchto dvou byla splněna.");
            sb.AppendLine("Tabulka 'Jinak' znamená, že pokud nebude splněna podmínka přejde se do této sekce.");
            sb.AppendLine("Zaškrtávací políčko 'Na konec' znamená, že po této sekci dojde k ukončení dotazníku.");
            sb.AppendLine();
            sb.AppendLine("Tyto čtyři tabulky spolu tvoří jednu tzv. podmínku, kterých můžeme pomocí tlačítka přidávat více. Vždy každá další podmínka" +
                " nahrazuje tabulku 'Jinak' u té předchozí, čili v případě, že předchozí podmínka není splněna, vyhodnotí se ta následujíci.");
            sb.AppendLine();
            sb.AppendLine("Příklad");
            sb.AppendLine("Budu mít dvě sekce: Sekce 1 - Pohlaví, Sekce 2 - Věk. V Sekci 1 bude otázka: 'Jaké je vaše pohlaví?' S možnostmi" +
                " muž, žena, nevím. Chceme aby pokud je to muž nebo žena půjde se na Sekci 2. Jinak 'Na konec'.");
            sb.AppendLine("Nastavení by vypadalo následovně:");
            sb.AppendLine("V tabulce 'Do sekce' by bylo zaškrtnuto Sekce 2 - Věk.");
            sb.AppendLine("V tabulce 'Pokud AND' by nebylo zaškrtnuto nic, protože chceme muže nebo ženu");
            sb.AppendLine("V tabulce 'Pokud OR' by bylo zaškrtnuto 'muž', 'žena'");
            sb.AppendLine("V tabulce 'Jinak' by bylo zaškrtnuto 'Na konec'");

            MessageBox.Show(sb.ToString(), "Nápověda");
        }

        protected override void AddElement(object parameter)
        {
            base.AddElement(parameter);
            UpdateProperty();
        }

        protected override void RemoveElement(object parameter)
        {
            base.RemoveElement(parameter);
            UpdateProperty();
        }

        private void UpdateProperty()
        {
            foreach (var ele in ElementsList)
            {
                ele.UpdateProperty();
            }
        }

        public List<SectionViewModel> GetAllConditionSections()
        {
            var returnList = new List<SectionViewModel>();
            foreach(var item in ElementsList)
            {
                if(item.SelectedToSection != null)
                {
                    returnList.Add(item.SelectedToSection);
                }
                if (item.SelectedElseSection != null)
                {
                    returnList.Add(item.SelectedElseSection);
                }
            }
            if(ElementsList.Count() == 0)
            {
                var sections = RelevantSection.OwnerSurvey.ElementsList;
                int indexOf = sections.IndexOf(RelevantSection);
                if (indexOf < sections.Count - 1)
                {
                    returnList.Add(sections.ElementAt(indexOf + 1));
                }
            }
            return returnList;
        }

        public SectionViewModel GetNextSection()
        {
            if (LogicOwner != null && LogicOwner.IsValidLogic)
            {
                foreach (var condition in ElementsList)
                {
                    var listOfCheckedAnswers = condition.AllTheAnswersInSection.Where(x => x.Checked);
                    if(condition.SelectedAndAnswers.Count() != 0 && condition.SelectedOrAnswers.Count() != 0)
                    {
                        bool andContained = !condition.SelectedAndAnswers.Except(listOfCheckedAnswers).Any();
                        bool orContained = false;
                        foreach (var selectedAnswer in condition.SelectedOrAnswers)
                        {
                            if (listOfCheckedAnswers.Contains(selectedAnswer))
                            {
                                orContained = true;
                            }
                        }

                        if (condition.AndCheck)
                        {
                            if(andContained && orContained)
                            {
                                return condition.SelectedToSection;
                            }
                            else
                            {
                                if (condition == ElementsList.Last())
                                {
                                    return condition.SelectedElseSection;
                                }
                            }
                        }
                        else if (condition.OrCheck)
                        {
                            if (andContained || orContained)
                            {
                                return condition.SelectedToSection;
                            }
                            else
                            {
                                if (condition == ElementsList.Last())
                                {
                                    return condition.SelectedElseSection;
                                }
                            }
                        }
                    }
                    if(condition.SelectedAndAnswers.Count() == 0)
                    {
                        bool orContained = false;
                        foreach (var selectedAnswer in condition.SelectedOrAnswers)
                        {
                            if (listOfCheckedAnswers.Contains(selectedAnswer))
                            {
                                orContained = true;
                            }
                        }
                        if (orContained)
                        {
                            return condition.SelectedToSection;
                        }
                        else
                        {
                            if (condition == ElementsList.Last())
                            {
                                return condition.SelectedElseSection;
                            }
                        }
                    }
                    else if(condition.SelectedOrAnswers.Count() == 0)
                    {
                        bool sameSize = false;
                        sameSize = condition.SelectedAndAnswers.Count() == listOfCheckedAnswers.Count();
                        if (sameSize)
                        {
                            bool andContained = !condition.SelectedAndAnswers.Except(listOfCheckedAnswers).Any();
                            if (andContained)
                            {
                                return condition.SelectedToSection;
                            }
                            else
                            {
                                if (condition == ElementsList.Last())
                                {
                                    return condition.SelectedElseSection;
                                }
                            }
                        }
                        else
                        {
                            if (condition == ElementsList.Last())
                            {
                                return condition.SelectedElseSection;
                            }
                        }
                    }
                }
            }
            var sections = RelevantSection.OwnerSurvey.ElementsList;
            int indexOf = sections.IndexOf(RelevantSection);
            if (indexOf < sections.Count - 1)
            {
                return sections.ElementAt(indexOf + 1);
            }
            return null;
        }
    }
}

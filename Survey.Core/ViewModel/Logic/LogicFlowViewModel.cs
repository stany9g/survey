﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class LogicFlowViewModel : BaseViewModel
    {
        public SurveyViewModel OwnerSurvey { get; set; }

        [JsonProperty]
        public ObservableCollection<ActualLogicPage> ConditionPages { get; set; } = new ObservableCollection<ActualLogicPage>();

        public ActualLogicPage LogicPage { get; set; }

        public ICommand LeftSectionCommand { get; private set; }

        public ICommand RightSectionCommand { get; private set; }

        public ICommand BackToCreatingCommand { get; private set; }

        public ICommand SaveLogicCommand { get; private set; }

        [JsonProperty]
        public bool IsValidLogic { get;  set; }

        public LogicFlowViewModel(SurveyViewModel ownerSurvey)
        {
            OwnerSurvey = ownerSurvey;

            var sectionsWithoutMain = OwnerSurvey.ElementsList.Where(x => x.IsMainSection == false).ToList();
            foreach(var section in sectionsWithoutMain)
            {
                ConditionPages.Add(new ActualLogicPage(this, section));
            }
            LogicPage = ConditionPages.First();

            InitCommands();

        }
        public LogicFlowViewModel()
        {
            InitCommands();
        }

        public void Update()
        {
            var sectionsWithoutMain = OwnerSurvey.ElementsList.Where(x => x.IsMainSection == false).ToList();
            var alreadySections = ConditionPages.Select(x => x.RelevantSection).ToList();

            var newSections = sectionsWithoutMain.Except(alreadySections).ToList();
            var oldSections = alreadySections.Except(sectionsWithoutMain).ToList();
            foreach(var sec in oldSections)
            {
               var actualPage = ConditionPages.Where(x => x.RelevantSection == sec).Single();
               ConditionPages.Remove(actualPage);
            }
            foreach (var sec in newSections)
            {
                ConditionPages.Add(new ActualLogicPage(this, sec));
            }

        }

        private void InitCommands()
        {
            LeftSectionCommand = new RelayCommand(LeftSection);
            RightSectionCommand = new RelayCommand(RightSection);
            BackToCreatingCommand = new RelayCommand(BackToCreating);
            SaveLogicCommand = new RelayCommand(SaveLogic);
        }

        private void SaveLogic()
        {
            StringBuilder sb = new StringBuilder();
            foreach(var item in ConditionPages)
            {
                foreach(var element in item.ElementsList)
                {
                    if (!element.IsValid)
                    {
                        sb.AppendLine(element.GetErrorMessage());
                    }
                }
            }
            int lengthBeforeCircularReference = sb.Length;
            CreateDependency(sb);
            if (lengthBeforeCircularReference == 0)
            {
                var yesOrNo = true;
                if (sb.Length != 0)
                {
                  yesOrNo = OwnerSurvey.OwnerSurveyManager.MainViewModel.GetYesOrNo(sb.ToString(), "Logický průchod uložen, jste si jist?");
                }
                else
                {
                    OwnerSurvey.OwnerSurveyManager.MainViewModel.GetConfirm("Logický průchod uložen", "");
                }
                if(yesOrNo)
                {
                    foreach (var logic in ConditionPages)
                    {
                        logic.RelevantSection.Logic = logic;
                    }
                    IsValidLogic = true;
                    EditingHelper.Instance.Editing = true;
                    EditingHelper.Instance.Logic = false;

                    OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = OwnerSurvey;
                }
            }
            else
            {
                OwnerSurvey.OwnerSurveyManager.MainViewModel.GetConfirm(sb.ToString(), "");
                IsValidLogic = false;
            }
            DataSerialization.Save(OwnerSurvey);
        }

        private void CreateDependency(StringBuilder sb)
        {
            var serviceDependence = new Dictionary<SectionViewModel, List<SectionViewModel>>();
            foreach(var item in ConditionPages)
            {
                serviceDependence.Add(item.RelevantSection, item.GetAllConditionSections());
            }
            var cycles = serviceDependence.FindCycles();
            if(cycles.Count != 0)
            {
                sb.AppendLine("Nastavení logiky obsahuje cyklus, prosím odstraňte ho, aby se respondent nedostal do smyčky.");
                cycles.ToList().ForEach(x =>
                {
                    var last = x.Last();
                    var secondLastIndex = x.Count() - 2;
                    var secondLast = x[secondLastIndex];
                    sb.AppendLine($"{secondLast.ToString()} vede do {last.ToString()}, která v sobě již nastavenou logiku má.");
                    sb.AppendLine($"Kdy může dojít k zacyklení: {string.Join(" ", x)}");
                });
            }

        }

        private void LeftSection()
        {
            var indexOf = ConditionPages.IndexOf(LogicPage);
            if (indexOf > 0)
            {
                LogicPage = ConditionPages.ElementAt(indexOf - 1);
            }
        }

        private void RightSection()
        {
            var indexOf = ConditionPages.IndexOf(LogicPage);
            if (indexOf < ConditionPages.Count - 1)
            {
                LogicPage = ConditionPages.ElementAt(indexOf + 1);
            }
        }

        private void BackToCreating()
        {
            var result = OwnerSurvey.OwnerSurveyManager.MainViewModel.GetYesOrNo("Logický průchod bude označen jako nevalidní.", "Opravdu chcete zpět?");
            if (result)
            {
                EditingHelper.Instance.Editing = true;
                EditingHelper.Instance.Logic = false;
                OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = OwnerSurvey;
                IsValidLogic = false;
                OwnerSurvey.ElementsList.ToList().ForEach(x => x.Logic = new ActualLogicPage { RelevantSection = x, LogicOwner = this });
            }
        }

    }
}

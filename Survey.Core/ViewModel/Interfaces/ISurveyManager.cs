﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    /// <summary>
    /// Encapsulate the process of components
    /// </summary>
    public interface ISurveyManager
    {
        /// <summary>
        /// Holds all the files (surveys) available to open
        /// </summary>
        ObservableCollection<ISurvey> Surveys { get; }

        /// <summary>
        /// Universally unique identifier
        /// </summary>
        string Uuid { get; }

        /// <summary>
        /// Creates a new survey
        /// </summary>
        ICommand CreateCommand { get; }

        /// <summary>
        /// Loads a survey from <paramref name="path"/> in .xml format
        /// </summary>
        /// <param name="path"></param>
        ICommand LoadCommand { get; }

        /// <summary>
        /// Save a survey
        /// </summary>
        ICommand SaveCommand { get;  }

        /// <summary>
        /// Edit a survey
        /// </summary>
        ICommand EditCommand { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public interface ISection : IElement
    {
        /// <summary>
        /// Holds all the available questions
        /// </summary>
        ObservableCollection<IQuestion> Questions { get; set; }

        /// <summary>
        /// Holds the header of the section
        /// </summary>
        SectionInfoViewModel SectionInfoViewModel { get; set; }

        /// <summary>
        /// Owner of the this section
        /// </summary>
        ISurvey OwnerSurvey { get; }

        /// <summary>
        /// Adds the question into the <see cref="Questions"/>
        /// </summary>
        ICommand AddQuestionCommand { get; }

        /// <summary>
        /// Deletes the question from the <see cref="Questions"/>
        /// </summary>
        ICommand DeleteQuestionCommand { get; }

        /// <summary>
        /// Edit question and update it in the list
        /// </summary>
        ICommand EditQuestionCommand { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    /// <summary>
    /// Question as a text
    /// </summary>
    public interface IQuestion : IElement
    {
        /// <summary>
        /// The section which this question is included
        /// </summary>
        ISection OwnerSection { get; set; }

        /// <summary>
        /// All options for the answer on this question
        /// </summary>
        ObservableCollection<IAnswer> Answers { get; set; }

        /// <summary>
        /// If question is optional and respondent does not have to answer
        /// </summary>
        bool Optional { get; }

        /// <summary>
        /// If we want to set the next question by its answer
        /// </summary>

        bool QuestionByAnswer { get; set; }

        /// <summary>
        /// Add new options for answer
        /// </summary>
        ICommand AddAnswerCommand { get; set; }

        /// <summary>
        /// Delete the option
        /// </summary>
        ICommand DeleteAnswerCommand { get; set; }

        /// <summary>
        /// Edit the option and update the list
        /// </summary>
        ICommand EditAnswerCommand { get; set; }
    }
}

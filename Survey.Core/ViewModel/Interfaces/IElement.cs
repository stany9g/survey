﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    /// <summary>
    /// A base interface for all used components.
    /// </summary>
    public interface IElement
    {
        /// <summary>
        /// An Id number.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Name/note of the element.
        /// </summary>
        string Text { get; }

        /// <summary>
        /// Indicates if element is valid
        /// </summary>
        bool IsValid { get; }
    }
}

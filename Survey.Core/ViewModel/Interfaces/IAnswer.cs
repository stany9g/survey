﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    /// <summary>
    /// Option to answer in a question
    /// </summary>
    public interface IAnswer : IElement
    {
        /// <summary>
        /// If the option is choosen by respondent
        /// </summary>
        bool Checked { get; set; }

        /// <summary>
        /// Holds history of question before
        /// </summary>
        IQuestion ParentQuestion { get; }

        /// <summary>
        /// Holds information of what next question should be
        /// </summary>
        IQuestion NextQuestion { get; }
    }
}

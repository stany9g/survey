﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    /// <summary>
    /// Encapsulates the survey
    /// </summary>
    public interface ISurvey : IElement
    {
        /// <summary>
        /// An introduction about survey
        /// </summary>
        string Description { get;  }

        /// <summary>
        /// Holds all the available questions
        /// </summary>
        ObservableCollection<ISection> Sections { get; set; }

        /// <summary>
        /// Owner of the this survey
        /// </summary>
        ISurveyManager OwnerSurveyManager { get; }

        /// <summary>
        /// Adds the question into the <see cref="Questions"/>
        /// </summary>
        ICommand AddSectionCommand { get; }

        /// <summary>
        /// Deletes the question from the <see cref="Questions"/>
        /// </summary>
        ICommand DeleteSectionCommand { get; }

        /// <summary>
        /// Edit question and update it in the list
        /// TODO promyslet si edit
        /// </summary>
        ICommand EditQuestionCommand { get; }
    }
}

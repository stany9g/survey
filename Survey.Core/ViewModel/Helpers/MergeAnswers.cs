﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public static class MergeAnswers
    {
        private static bool _reported = false;
        public static void Merge(SurveyViewModel survey, string filePath)
        {
            var answers = GetSurveys(survey, filePath);
            if(answers.Count() != 0)
            {
                SetRightSectionsAndAnswers(survey, answers);
                survey.OwnerSurveyManager.MainViewModel.GetConfirm("Odpovědi přidány.", "Ok");
                survey.UpdateProperty();

                DataSerialization.Save(survey);
            }
            else
            {
                if(!_reported)
                {
                    survey.OwnerSurveyManager.MainViewModel.GetConfirm("Žádné nové odpovědi nebyly přidány.", "Info");
                }
            }
        }

        private static IEnumerable<RespondentAnswers> GetDistinctAnswers(SurveyViewModel survey, IEnumerable<SurveyViewModel> surveys)
        {
            var returnList = new List<RespondentAnswers>();
            if (surveys.Count() != 0)
            {
                foreach (var forMergeSurvey in surveys)
                {
                    if (survey.Uuid == forMergeSurvey.Uuid)
                    {
                        foreach (var surveyAnswer in forMergeSurvey.RespondentAnswers)
                        {
                            int same = -1;
                            foreach (var originSurveyAnswer in survey.RespondentAnswers)
                            {
                                var comparator = new ComparatorByRespondentAnswer();
                                same = comparator.Compare(surveyAnswer, originSurveyAnswer);
                            }
                            if (same == -1)
                            {
                                survey.RespondentAnswers.Add(surveyAnswer);
                                returnList.Add(surveyAnswer);
                            }
                        }
                    }
                }
                return returnList;
            }
            else
            {
                survey.OwnerSurveyManager.MainViewModel.GetConfirm("Nenalezeny žádné dotazníky s odpovědmi.", "Chyba");
                _reported = true;
                return new List<RespondentAnswers>();
            }
        }

        private static IEnumerable<RespondentAnswers> GetSurveys(SurveyViewModel survey, string filePath)
        {
            if (Directory.Exists(filePath))
            {
                var files = Directory.GetFiles(filePath, "*.json");
                var returnList = new List<SurveyViewModel>();
                foreach (var file in files)
                {
                    string passPhrase = "NaplavaJeKing";
                    string output = File.ReadAllText(file);
                    string decrypted = Encryption.Decrypt(output, passPhrase);
                    try
                    {
                        var loadedSurvey = JsonConvert.DeserializeObject<SurveyViewModel>(decrypted, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Error });
                        if(loadedSurvey.Uuid == survey.Uuid)
                        {
                            returnList.Add(loadedSurvey);
                        }
                    }
                    catch (Exception)
                    {
                        survey.OwnerSurveyManager.MainViewModel.GetConfirm($"Nevalidní soubor s odpovědmi: {file}", "Chyba");
                        _reported = true;
                    }
                }
                SetOwners(survey, returnList);
                return GetDistinctAnswers(survey, returnList);
            }
            else
            {
                _reported = true;
                survey.OwnerSurveyManager.MainViewModel.GetConfirm("Neexistující složka.", "Chyba");
                return new List<RespondentAnswers>();
            }
        }
        private static void SetOwners(SurveyViewModel ownerSurvey,IEnumerable<SurveyViewModel> surveys)
        {
            foreach (var survey in surveys)
            {
                survey.OwnerSurveyManager = ownerSurvey.OwnerSurveyManager;
                survey.SetOwnersLogicRight();
                survey.ActiveSection = survey.ElementsList.First();
                foreach (var info in survey.RespondentInfo)
                {
                    info.OwnerSurvey = survey;
                }
                foreach (var answer in survey.RespondentAnswers)
                {
                    foreach (var info in answer.RespondentInfo)
                    {
                        info.OwnerSurvey = survey;
                    }
                }
                foreach (var section in survey.ElementsList)
                {
                    section.OwnerSurvey = survey;
                    section.SectionInfoViewModel.OwnerSection = section;
                    foreach (var question in section.ElementsList)
                    {
                        question.OwnerSection = section;
                        foreach (var answer in question.ElementsList)
                        {
                            answer.ParentQuestion = question;
                        }
                    }
                }

            }
        }

        private static void SetRightSectionsAndAnswers(SurveyViewModel ownerSurvey, IEnumerable<RespondentAnswers> respondentAnswers)
        {
            foreach(var answer in respondentAnswers)
            {
                var stashSection = new List<SectionViewModel>();
                foreach(var section in answer.WalkTrought)
                {
                    var findSection = ownerSurvey.ElementsList.ToList().Find(x => x.Text == section.Text && x.Name == section.Name && x.Id == section.Id);
                    stashSection.Add(findSection);
                }
                answer.WalkTrought = stashSection;
                var answers = ownerSurvey.ElementsList
                       .SelectMany(x => x.ElementsList)
                       .SelectMany(x => x.ElementsList)
                       .ToList();
                var stashAnswer = new List<AnswerViewModel>();
                foreach (var CheckedAnswer in answer.CheckedAnswers)
                {
                    var findAnswer = answers.Find(x => x.Text == CheckedAnswer.Text && x.Id == CheckedAnswer.Id &&
                    x.ParentQuestion.Text == CheckedAnswer.ParentQuestion.Text && x.ParentQuestion.Id == CheckedAnswer.ParentQuestion.Id);
                    stashAnswer.Add(findAnswer);
                }
                answer.CheckedAnswers = stashAnswer;
                foreach(var respondentInfo in answer.RespondentInfo)
                {
                    respondentInfo.OwnerSurvey = ownerSurvey;
                }

                var questions = ownerSurvey.ElementsList.SelectMany(x => x.ElementsList).ToList();

                var questioncopy = new List<QuestionViewModel>();
                answer.CheckedAnswers.ToList().ForEach(x => questioncopy.Add(x.ParentQuestion));

                foreach(var q in questioncopy)
                {
                    var find = questions.Find(x => x.Text == q.Text);
                    bool t = ReferenceEquals(find, q);
                    if(t == false)
                    {
                        throw new Exception("Bad reference");
                    }
                }
            }
        }
    }
}

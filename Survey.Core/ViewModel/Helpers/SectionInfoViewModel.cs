﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptOut)]
    public class SectionInfoViewModel : BaseViewModel
    {
        [JsonIgnore]
        public bool EmptyName { get { return !string.IsNullOrEmpty(Name); } }

        [JsonIgnore]
        public SectionViewModel OwnerSection;

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\SurveysData";
                if(OwnerSection != null && OwnerSection.OwnerSurvey != null)
                {
                    string filePath = Path.Combine(dir, OwnerSection.OwnerSurvey.Text + " " + OwnerSection.OwnerSurvey.Uuid + ".json");
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);

                    }
                }
                _name = value;


            }
        }

        public string TagName { get; set; }

        [JsonIgnore]
        public bool EmptyDescription{ get { return !string.IsNullOrEmpty(Description); } }

        public string Description { get; set; }

        public string TagDescription { get; set; }

        public SectionInfoViewModel(string tagName, string tagDescription, SectionViewModel ownerSection)
        {
            TagName = tagName;
            TagDescription = tagDescription;
            OwnerSection = ownerSection;
        }



        public SectionInfoViewModel()
        {

        }
    }
}

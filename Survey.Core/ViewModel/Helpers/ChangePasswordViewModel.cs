﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public class ChangePasswordViewModel : BaseViewModel
    {
        private PasswordViewModel _passwordViewModel;

        public string OldPass { get; set; }

        public string NewPass { get; set; }

        public string NewPassAgain { get; set; }

        public ICommand ChangePassCommand { get; set; }

        public ICommand BackCommand { get; set; }

        public bool IsAvailable
        {
            get
            {
                return !string.IsNullOrEmpty(OldPass) && !string.IsNullOrEmpty(NewPass) && !string.IsNullOrEmpty(NewPassAgain);
            }
        }


        public ChangePasswordViewModel(PasswordViewModel ownerPassword)
        {
            _passwordViewModel = ownerPassword;

            ChangePassCommand = new RelayCommand(ChangePass);
            BackCommand = new RelayCommand(Back);
        }

        private void Back()
        {
            _passwordViewModel.SurveyManager.MainViewModel.CurrentPageViewModel = _passwordViewModel;
        }

        private void ChangePass()
        {
            if(OldPass == Properties.Settings.Default.Password)
            {
                if(NewPass != NewPassAgain)
                {
                    _passwordViewModel.SurveyManager.MainViewModel.GetConfirm("Nové heslo se neshoduje s Nové heslo znovu", "Chyba");
                }
                else
                {
                    Properties.Settings.Default.Password = NewPass;
                    Properties.Settings.Default.Save();
                    bool result = _passwordViewModel.SurveyManager.MainViewModel.GetYesOrNo("Chcete přejít do editoru?", "Heslo změněno");
                    if (result)
                    {
                        _passwordViewModel.SurveyManager.MainViewModel.CurrentPageViewModel = _passwordViewModel.SurveyManager;
                    }
                    else
                    {
                        _passwordViewModel.SurveyManager.MainViewModel.CurrentPageViewModel = _passwordViewModel;
                    }
                }
            }
            else
            {
                _passwordViewModel.SurveyManager.MainViewModel.GetConfirm("Staré heslo nesouhlasí.", "Chyba");
            }
        }
    }
}

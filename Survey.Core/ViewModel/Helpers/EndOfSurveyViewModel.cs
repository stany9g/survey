﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public class EndOfSurveyViewModel : BaseViewModel
    {
        public SurveyViewModel OwnerSurvey { get; set; }

        public ICommand BackCommand { get; set; }

        public EndOfSurveyViewModel(SurveyViewModel ownerSurvey, IEnumerable<SectionViewModel> walkThrough)
        {
            OwnerSurvey = ownerSurvey;
            SaveRespondentAnswers(walkThrough);
            BackCommand = new RelayCommand(GoBack);
        }

        public void SaveRespondentAnswers(IEnumerable<SectionViewModel> walkThrough)
        {
            var answers = OwnerSurvey.ElementsList
                .SelectMany(x => x.ElementsList)
                .SelectMany(x => x.ElementsList)
                .Where(x => x.Checked == true)
                .ToList();

            answers.ForEach(x => x.Checked = false);
            var newRespondentInfo = new ObservableCollection<RespondentInfo>();
            OwnerSurvey.RespondentInfo.ToList().ForEach(x =>
            {
                newRespondentInfo.Add(x.DeepCopy(x));
                x.RespondentText = "";
            });

            OwnerSurvey.RespondentAnswers.Add(new RespondentAnswers(walkThrough, answers, newRespondentInfo));

            DataSerialization.Save(OwnerSurvey);
        }

        private void GoBack()
        {
            if(EditingHelper.Instance.Respondent == false)
            {
                OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = OwnerSurvey.OwnerSurveyManager;
            }
            else
            {
                OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = new StartOfSurveyViewModel(OwnerSurvey.OwnerSurveyManager);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class EditingHelper
    {
        public bool Editing { get; set; } = false;

        public bool Logic { get; set; } = false;

        public bool Respondent { get; set; } = true;

        public bool Results { get; set; } = false;

        public static EditingHelper Instance { get; } = new EditingHelper();
    }
}

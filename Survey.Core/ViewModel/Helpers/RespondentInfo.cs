﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class RespondentInfo : BaseViewModel
    {
        public string LabelText { get; set; }

        public bool IsValid { get; set; } = true;

        private string _respondentText;
        public string RespondentText
        {
            get
            {
                return _respondentText;
            }
            set
            {
                _respondentText = value;
                if(OwnerSurvey != null)
                {
                    OnPropertyChanged(nameof(RequiredHasAnswerCheck));
                }
            }
        }

        public bool RequiredHasAnswerCheck
        {
            get
            {
                if (Required)
                {
                    return string.IsNullOrEmpty(RespondentText);
                }
                return false;
            }
        }

        public bool Required { get; set; } = true;


        [JsonIgnore]
        public SurveyViewModel OwnerSurvey { get;  set; }

        public RespondentInfo(SurveyViewModel owner)
        {
            OwnerSurvey = owner;
        }

        public RespondentInfo()
        {

        }

        public RespondentInfo DeepCopy(RespondentInfo source)
        {
            return new RespondentInfo(source.OwnerSurvey)
            {
                LabelText = source.LabelText,
                RespondentText = source.RespondentText,
                Required = source.Required
            };
        }
    }
}

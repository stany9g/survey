﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public class PasswordViewModel : BaseViewModel
    {
        public SurveyManagerViewModel SurveyManager { get; set; }

        public StartOfSurveyViewModel FromStart { get; set; }

        public ICommand BackCommand { get; set; }

        public ICommand EditorCommand { get; set; }

        public ICommand ChangePassCommand { get; set; }

        public string Password { get; set; }

        public PasswordViewModel(StartOfSurveyViewModel from, SurveyManagerViewModel to)
        {
            SurveyManager = to;
            FromStart = from;

            EditorCommand = new RelayCommand(Editor);
            BackCommand = new RelayCommand(Back);
            ChangePassCommand = new RelayCommand(ChangePass);
        }

        private void ChangePass()
        {
            SurveyManager.MainViewModel.CurrentPageViewModel = new ChangePasswordViewModel(this);
        }

        private void Editor()
        {
            //string pass = "1234";
            string pass = Properties.Settings.Default.Password;
            if (Password != null)
            {
                if (Password.Equals(pass))
                {
                    EditingHelper.Instance.Respondent = false;
                    SurveyManager.MainViewModel.CurrentPageViewModel = SurveyManager;
                }
                else
                {
                    SurveyManager.MainViewModel.GetConfirm("Nesprávné heslo", "Chyba");
                }
            }
            else
            {
                SurveyManager.MainViewModel.GetConfirm("Nezadal jste žádné heslo.", "Chyba");
            }
        }

        private void Back()
        {
            SurveyManager.MainViewModel.CurrentPageViewModel = FromStart;
        }
    }
}

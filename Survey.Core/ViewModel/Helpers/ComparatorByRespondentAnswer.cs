﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class ComparatorByRespondentAnswer : IComparer<RespondentAnswers>
    {
        public int Compare(RespondentAnswers x, RespondentAnswers y)
        {
            if (x.RespondentInfo.Count() != y.RespondentInfo.Count())
            {
                return -1;
            }
            for (int i = 0; i < x.RespondentInfo.Count(); i++)
            {
                if(x.RespondentInfo[i].RespondentText != y.RespondentInfo[i].RespondentText)
                {
                    return -1;
                }
            }
            return 0;
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public class StartOfSurveyViewModel : BaseViewModel
    {
        public SurveyManagerViewModel SurveyManager { get; set; }

        public ICommand StartCommand { get; set; }

        public ICommand EditorCommand { get; set; }


        public StartOfSurveyViewModel(SurveyManagerViewModel surveyManager)
        {
            SurveyManager = surveyManager;
            StartCommand = new RelayCommand(Start);
            EditorCommand = new RelayCommand(Editor);

            //Start();
        }

        private void Editor()
        {
            SurveyManager.MainViewModel.CurrentPageViewModel = new PasswordViewModel(this, SurveyManager);
        }

        private void Start()
        {
            var survey = SurveyManager.ElementsList.Where(x => x.ReadyForRespondent == true);
            if (survey.Count() == 0)
            {
                SurveyManager.MainViewModel.GetConfirm("Není vybrán žádný dotazník pro vyplnění.", "");
            }
            else
            {
                var answers = survey.Single().ElementsList
                   .SelectMany(x => x.ElementsList)
                   .SelectMany(x => x.ElementsList)
                   .ToList();
                answers.ForEach(x => x.Checked = false);
                foreach (var respondentInfo in survey.Single().RespondentInfo)
                {
                    respondentInfo.RespondentText = "";
                }
                SurveyManager.ShowAsRespondentCommand.Execute(survey.Single());
            }
        }
    }
}

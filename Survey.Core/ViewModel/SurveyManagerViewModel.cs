﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptIn)]
    public class SurveyManagerViewModel : BaseDataModel<SurveyViewModel>
    {
        public MainMenuViewModel MainViewModel { get; set; }

        public SurveyViewModel SelectedSurvey { get; set; }

        public ICommand SaveCommand { get; private set; }
        public ICommand BackCommand { get; private set; }
        public ICommand EditCommand { get; private set; }
        public ICommand ShowAsRespondentCommand { get; private set; }
        public ICommand LogicFlowCommand { get; set; }
        public ICommand PrepareForRespondentCommand { get; set; }
        public ICommand ShowResultsCommand { get; set; }
        public ICommand DeleteSelectedSurveyCommand { get; set; }

        public SurveyManagerViewModel(MainMenuViewModel mainViewModel) : base()
        {
            MainViewModel = mainViewModel;
            InitCommands();
        }

        public SurveyManagerViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            SaveCommand = new RelayParameterizedCommand(parameter => Save(parameter));
            EditCommand = new RelayParameterizedCommand(parameter => Edit(parameter));
            ShowAsRespondentCommand = new RelayParameterizedCommand(parameter => ShowAsResponder(parameter));
            BackCommand = new RelayParameterizedCommand(parameter => Back(parameter));
            LogicFlowCommand = new RelayParameterizedCommand(parameter => LogicFlow(parameter));
            PrepareForRespondentCommand = new RelayCommand(PrepareForRespondent);
            ShowResultsCommand = new RelayParameterizedCommand(parameter => ShowResults(parameter));
            DeleteSelectedSurveyCommand = new RelayCommand(DeleteSelectedSurvey);
        }

        private void DeleteSelectedSurvey()
        {
            if(SelectedSurvey != null)
            {
                if(MainViewModel.GetYesOrNo("Vybraný dotazník bude smazán.", "Jste si jist, že ho chcete smazat?"))
                {
                    string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\SurveysData";
                    string filePath = Path.Combine(dir, SelectedSurvey.Text + " "+ SelectedSurvey.Uuid + ".json");
                    if (File.Exists(filePath))
                    {
                        RemoveElement(SelectedSurvey);
                        File.Delete(filePath);
                    }
                }
            }
        }

        private void ShowResults(object parameter)
        {
            var survey = parameter as SurveyViewModel;
            EditingHelper.Instance.Results = true;
            MainViewModel.CurrentPageViewModel = new RespondentAnswersListViewModel(survey);
        }

        private void PrepareForRespondent()
        {
            EditingHelper.Instance.Respondent = true;
            MainViewModel.CurrentPageViewModel = new StartOfSurveyViewModel(this);
        }

        private void LogicFlow(object parameter)
        {
            var survey = parameter as SurveyViewModel;
            if (survey.IsValid)
            {
                EditingHelper.Instance.Editing = false;
                EditingHelper.Instance.Logic = true;
                if (survey.Logic == null)
                {
                    survey.Logic = new LogicFlowViewModel(survey);
                }
                else
                {
                    survey.Logic.Update();
                }
                MainViewModel.CurrentPageViewModel = survey.Logic;
            }
            else
            {
                MainViewModel.GetConfirm(survey.GetErrorMsg(), "Chyba");
            }
        }

        private void ShowAsResponder(object parameter)
        {
            var survey = parameter as SurveyViewModel;
            EditingHelper.Instance.Editing = false;
            survey.ActiveSection = survey.ElementsList.First();
            MainViewModel.CurrentPageViewModel = survey;
        }

        private void Edit(object parameter)
        {
            var survey = parameter as SurveyViewModel;

            EditingHelper.Instance.Editing = true;

            survey.ActiveSection = survey.ElementsList.First();

            MainViewModel.CurrentPageViewModel = survey;
        }

        private void Back(object parameter)
        {
            if (EditingHelper.Instance.Editing)
            {
                EditingHelper.Instance.Editing = false;
                MainViewModel.CurrentPageViewModel = this;
                DataSerialization.Save(parameter as SurveyViewModel);
            }
        }


        protected override void AddElement(object parameter)
        {

            var surveyViewModel = new SurveyViewModel(this);
            ElementsList.Add(surveyViewModel);

            EditingHelper.Instance.Editing = true;
            UpdateId();

            // This change the data context of the page
            MainViewModel.CurrentPageViewModel = surveyViewModel;
        }


        private void Save(object parameter)
        {
            var survey = parameter as SurveyViewModel;
            DataSerialization.Save(survey);
            if (survey.IsValid)
            {
                bool resultFromMsg = MainViewModel.GetConfirm("Dotazník je připraven na vyplnění.", "Uloženo");
                if (resultFromMsg)
                {
                    MainViewModel.CurrentPageViewModel = this;
                    EditingHelper.Instance.Editing = false;
                }
            }
            else
            {
                MainViewModel.GetConfirm(survey.GetErrorMsg(),"Chyba");
            }
        }
    }
}

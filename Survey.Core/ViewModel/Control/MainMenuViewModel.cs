﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public class MainMenuViewModel : BaseViewModel
    {
        private readonly Func<string, string, bool> _confirmMessageBoxFunc;

        private readonly Func<string, string, bool> _yesnoMessageBoxFunc;

        public BaseViewModel CurrentPageViewModel { get; set; }

        public MainMenuViewModel(Func<string, string, bool> confirm, Func<string, string, bool> yesno)
        {
            _confirmMessageBoxFunc = confirm;
            _yesnoMessageBoxFunc = yesno;
            var surveyManager  = new SurveyManagerViewModel(this);
            var list = DataSerialization.Load();
            if (list != null)
            {
                surveyManager.ElementsList = new ObservableCollection<SurveyViewModel>(list);
                SetOwners(surveyManager);
            }
            if(EditingHelper.Instance.Respondent)
            {
                CurrentPageViewModel = new StartOfSurveyViewModel(surveyManager);
            }
            else
            {
                CurrentPageViewModel = surveyManager;
            }
        }

        public bool GetConfirm(string message, string caption)
        {
            return _confirmMessageBoxFunc(message, caption);
        }

        public bool GetYesOrNo(string message, string caption)
        {
            return _yesnoMessageBoxFunc(message, caption);
        }

        private void SetOwners(SurveyManagerViewModel manager)
        {
            foreach (var survey in manager.ElementsList)
            {
                survey.OwnerSurveyManager = manager;
                survey.SetOwnersLogicRight();
                survey.ActiveSection = survey.ElementsList.First();
                foreach(var info in survey.RespondentInfo)
                {
                    info.OwnerSurvey = survey;
                }
                foreach (var answer in survey.RespondentAnswers)
                {
                    foreach(var info in answer.RespondentInfo)
                    {
                        info.OwnerSurvey = survey;
                    }
                }
                foreach (var section in survey.ElementsList)
                {
                    section.OwnerSurvey = survey;
                    section.SectionInfoViewModel.OwnerSection = section;
                    foreach(var question in section.ElementsList)
                    {
                        question.OwnerSection = section;
                        foreach(var answer in question.ElementsList)
                        {
                            answer.ParentQuestion = question;
                        }
                    }
                }
                /*
                var answers = survey.ElementsList
                       .SelectMany(x => x.ElementsList)
                       .SelectMany(x => x.ElementsList)
                       .ToList();
                foreach (var respondentAnswer in survey.RespondentAnswers)
                {
                    var list = new ObservableCollection<AnswerViewModel>();
                    foreach(var CheckedAnswer in respondentAnswer.CheckedAnswers)
                    {
                       var findAnswer = answers.Find(x => x.Text == CheckedAnswer.Text);
                        list.Add(findAnswer);
                    }
                    respondentAnswer.CheckedAnswers = list;
                    var sections = survey.ElementsList.ToList();
                    var listSec = new ObservableCollection<SectionViewModel>();
                    foreach (var section in respondentAnswer.WalkTrought)
                    {
                        var findSection = sections.Find(x => x.Text == section.Text);
                        listSec.Add(findSection);
                    }
                    respondentAnswer.WalkTrought = listSec;
                }*/
            }

        }

    }
}

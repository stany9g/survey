﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    public class RespondentAnswersListViewModel : BaseViewModel
    {
        public ObservableCollection<RespondentAnswers> RespondentAnswers { get;  set; }

        public SurveyViewModel OwnerSurvey { get; set; }

        public RespondentAnswers ActiveRespondent { get; set; }

        public ICommand LeftSectionCommand { get; private set; }

        public ICommand RightSectionCommand { get; private set; }

        public ICommand RemoveRespondentAnswerCommand { get; private set; }

        public string Text
        {
            get
            {
                return $"{RespondentAnswers.IndexOf(ActiveRespondent)+1}/{RespondentAnswers.Count}";
            }
        }


        public RespondentAnswersListViewModel(SurveyViewModel ownerSurvey)
        {
            OwnerSurvey = ownerSurvey;
            RespondentAnswers = OwnerSurvey.RespondentAnswers;
            LeftSectionCommand = new RelayCommand(LeftSection);
            RightSectionCommand = new RelayCommand(RightSection);
            RemoveRespondentAnswerCommand = new RelayParameterizedCommand(parameter => RemoveRespondentAnswer(parameter));
            foreach (var item in RespondentAnswers)
            {
                item.OwnerList = this;
            }
            ActiveRespondent = RespondentAnswers.First();
            SetCheckedAnswers(true);
        }

        private void RightSection()
        {
            var indexOf = RespondentAnswers.IndexOf(ActiveRespondent);
            if (indexOf < RespondentAnswers.Count - 1)
            {
                SetCheckedAnswers(false);
                ActiveRespondent = RespondentAnswers.ElementAt(indexOf + 1);
                SetCheckedAnswers(true);
            }
        }

        private void LeftSection()
        {
            var indexOf = RespondentAnswers.IndexOf(ActiveRespondent);
            if (indexOf > 0)
            {
                SetCheckedAnswers(false);
                ActiveRespondent = RespondentAnswers.ElementAt(indexOf - 1);
                SetCheckedAnswers(true);
            }
        }

        private  void RemoveRespondentAnswer(object parameter)
        {
            bool res = OwnerSurvey.OwnerSurveyManager.MainViewModel.GetYesOrNo("Opravdu chcete smazat odpovědi vyplněné uživatelem?", "Jste si jist?");
            if (res)
            {
                SetCheckedAnswers(false);
                var respondentAnswer = parameter as RespondentAnswers;
                var indexOfDeleted = RespondentAnswers.IndexOf(respondentAnswer);

                RespondentAnswers.Remove(respondentAnswer);
                if (indexOfDeleted < RespondentAnswers.Count)
                {
                    ActiveRespondent = RespondentAnswers.ElementAt(indexOfDeleted);
                    SetCheckedAnswers(true);
                }
                else
                {
                    if (RespondentAnswers.Count != 0)
                    {
                        ActiveRespondent = RespondentAnswers.Last();
                        SetCheckedAnswers(true);

                    }
                    else
                    {
                        OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = OwnerSurvey.OwnerSurveyManager;
                        EditingHelper.Instance.Results = false;
                    }
                }
                DataSerialization.Save(OwnerSurvey);
            }
        }

        private void SetCheckedAnswers(bool value)
        {
            ActiveRespondent.CheckedAnswers.ToList().ForEach(x => x.Checked = value);
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Survey.Core
{
    [JsonObject(MemberSerialization.OptOut)]
    public class RespondentAnswers : BaseViewModel
    {

        public ObservableCollection<RespondentInfo> RespondentInfo { get;  set; }

        public IEnumerable<AnswerViewModel> CheckedAnswers { get;  set; }

        public IEnumerable<SectionViewModel> WalkTrought { get;  set; }

        [JsonIgnore]
        public IEnumerable<QuestionViewModel> ElementsList
        {
            get
            {
               return WalkTrought.SelectMany(x => x.ElementsList);
            }
        }
        [JsonIgnore]
        public ICommand BackCommand { get; set; }

        [JsonIgnore]
        public RespondentAnswersListViewModel OwnerList { get; set; }

        public RespondentAnswers(IEnumerable<SectionViewModel> walkThrough, IEnumerable<AnswerViewModel> answers, ObservableCollection<RespondentInfo> respondentInfo)
        {
            CheckedAnswers = answers;
            RespondentInfo = respondentInfo;
            WalkTrought = walkThrough;
            InitCommands();
        }

        public void SaveReport(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                ExportToCsv.CreateCsv(filePath, this);
            }
            else
            {
                OwnerList.OwnerSurvey.OwnerSurveyManager.MainViewModel.GetConfirm("Nebyla zvolena cesta.", "Chyba");
            }
        }

        public RespondentAnswers()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            BackCommand = new RelayCommand(Back);
        }

        private void Back()
        {
            EditingHelper.Instance.Results = false;
            OwnerList.ActiveRespondent.CheckedAnswers.ToList().ForEach(x => x.Checked = false);
            OwnerList.OwnerSurvey.OwnerSurveyManager.MainViewModel.CurrentPageViewModel = OwnerList.OwnerSurvey.OwnerSurveyManager;
        }



    }
}
